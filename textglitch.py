# textglitch: glitching text
# Copyright 2019, Luna Mendes and the textglitch contributors
# SPDX-License-Identifier: GPL-3.0-only

import sys
import argparse

from glitch_model import GlitchTextModel

def _mkparser() -> argparse.ArgumentParser:
    """Generate a command line parser."""
    parser = argparse.ArgumentParser()

    parser.add_argument('--rate', dest='rate',
                        help='glitching rate', default=0.3)

    return parser


def glitch_text(text: str, *, rate: float = 1) -> str:
    """Glitch text.

    Parameters
    ----------
    text: str
        The text to be glitched.
    glitch_coefficient: float, optional
        The coefficient representing the amount of glitch the text
        will receive, a value of 1 is random-output.
    """
    model = GlitchTextModel(rate)
    return model, model.work(text)


def main():
    """Main entrypoint"""
    parser = _mkparser()
    options = parser.parse_args()

    print('insert text to be glitched, Ctrl+D to finish insertion')
    input_text = []

    try:
        while True:
            input_text.append(
                input('> ')
            )
    except EOFError:
        input_text = '\n'.join(input_text)

    model, glitched = glitch_text(
        input_text, rate=float(options.rate)
    )

    print('\n--- input ---\n')
    print(input_text)

    print('\n--- output from', model, '---\n')
    print('\nlen:', len(glitched))
    print(glitched)


if __name__ == '__main__':
    main()
