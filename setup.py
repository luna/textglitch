from setuptools import setup

setup(
    name='textglitch',
    version='0.1',
    py_modules=['textglitch'],
    entry_points='''
        [console_scripts]
        textglitch=textglitch:main
    ''',
)

