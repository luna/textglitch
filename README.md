# glitch

experimentation with glitching text in python

## usage

requires python 3.6 or higher due to `random` library changes

```sh
git clone https://gitlab.com/luna/textglitch.git

cd textglitch

# make sure you got pip's preffered path for --user
pip install --user --editable .

# running with default glitch rate (30%)
textglitch

# set custom glitch rates! (10% for this one)
textglitch --rate 0.1

# you can also pipe
echo 'hehehe pene' | textglitch --rate 0.7
```
