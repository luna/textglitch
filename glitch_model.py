# textglitch: glitching text
# Copyright 2019, Luna Mendes and the textglitch contributors
# SPDX-License-Identifier: GPL-3.0-only

import string
import random
from typing import List


def _random_case(letter: str) -> str:
    func = random.choice([str.lower, str.upper])
    return func(letter)


def _split_text(text):
    for word in text.split():
        yield word
        yield ' '

LETTERS_WITH_COMBINE = list('aeious')

SPECIAL_MAPPINGS = list(
    '\N{Combining Grave Accent}'
    '\N{Combining Acute Accent}'
    '\N{Combining Circumflex Accent}'
    '\N{Combining Tilde}'
    '\N{Combining Overline}'
    '\N{Combining Left Angle Above}'
)

def _mkpools(char: str) -> List[str]:
    printable = string.printable
    punct = string.punctuation

    # subset of the printable characters around the given char
    p_idx = printable.find(char)
    pool_stage_1 = [char]
    pool_stage_1.extend(list(printable[p_idx - 2:p_idx + 2]))

    if char in LETTERS_WITH_COMBINE:
        pool_stage_1.extend(
            map(lambda comb: char + comb, SPECIAL_MAPPINGS)
        )

    # random subset of punctuations
    idx = random.randint(0, len(punct))
    subset_range = random.randint(0, 5) * random.choice([1, -1])

    # merge punctuation subset with printable char subset
    pool_stage_2 = list(punct[idx:idx + subset_range][:8])
    pool_stage_2.extend(pool_stage_1)

    # shuffle() but returns the given list back, instead of being in-place
    return [
        [char],
        pool_stage_1,
        random.sample(pool_stage_2, k=len(pool_stage_2))
    ]


def _glitch_char(char: str) -> str:
    """Glitch a single character."""
    res = None

    # currently only the stage 2 pool is used.
    pools = _mkpools(char)
    res = random.choice(pools[2])

    # if it is whitespace, remove it 30% of the time
    if char == ' ':
        res = random.choices(
            [' ', ''],
            [0.7, 0.3],
            k=1
        )[0]

    return res


def _raw_char_mode(word):
    # basically map every character in word to _glitch_char
    return ''.join(map(_glitch_char, word))


def _repetition_mode(word):
    # repeat raw_char_mode some N number of times
    res = []

    for _ in range(random.randint(0, 5)):
        res.append(
            _raw_char_mode(word)
        )

    return ' '.join(res)


class GlitchTextModel:
    """Text modeler for glitch text."""
    def __init__(self, rate):
        self.rate = rate

    def __repr__(self):
        return f'<GlitchTextModel rate={self.rate}>'

    def _work_word(self, word: str) -> str:
        # select a glitch mode for word, if its set to be glitched
        if random.random() < self.rate:
            mode = random.choices([
                _raw_char_mode,
                _repetition_mode
            ], [1, 0.2], k=1)[0]

            # hardcode raw char mode for whitespace
            if word == ' ':
                return _raw_char_mode(word)

            return mode(word)
        else:
            return word

    def work(self, text: str) -> str:
        """Glitch text."""

        result = []

        for word in _split_text(text):
            result.append(self._work_word(word))

        return ''.join(result)
